FROM openjdk:8u212-jdk-alpine3.9

ARG APP_VERSION

RUN \
# Check for mandatory build arguments
    : "${APP_VERSION:?Build argument APP_VERSION needs to be set and non-empty.}"
    
LABEL VERSION=${APP_VERSION}

ADD theme-enabler-services/target/theme-enabler-services-${APP_VERSION}.jar /app.jar

ENTRYPOINT ["java", "-jar", "/app.jar"]
