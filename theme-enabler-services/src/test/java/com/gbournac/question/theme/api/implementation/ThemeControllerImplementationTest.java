package com.gbournac.question.theme.api.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.gbournac.question.common.services.exception.FunctionalException;
import com.gbournac.question.common.services.exception.TechnicalException;
import com.gbournac.question.theme.domain.Language;
import com.gbournac.question.theme.domain.dto.CreateTheme;
import com.gbournac.question.theme.domain.dto.Theme;
import com.gbournac.question.theme.services.ThemeService;

/**
 * The class to test the theme controller
 *
 * @author Guénaël Bournac
 *
 */
class ThemeControllerImplementationTest {

	/**
	 * The controller under test
	 */
	ThemeControllerImplementation controller;

	/**
	 * A mocked theme service
	 */
	ThemeService mockThemeService;

	/**
	 * A mocked theme
	 */
	Theme mockTheme;

	/**
	 * A create theme dto object
	 */
	CreateTheme createTheme;

	@BeforeEach
	void setUp() throws Exception {
		this.mockThemeService = Mockito.mock(ThemeService.class);
		this.mockTheme = Mockito.mock(Theme.class);

		this.controller = new ThemeControllerImplementation();
		this.controller.setThemeService(this.mockThemeService);

		this.createTheme = CreateTheme.builder().language(Language.FR).name("a").build();
	}

	/**
	 * test the addTheme function in normal conditions
	 */
	@Test
	@DisplayName("test the addTheme function in normal conditions")
	void addThemeOk() throws Exception {
		Mockito.doReturn(this.mockTheme).when(this.mockThemeService).createTheme(Mockito.any());
		final ResponseEntity<Theme> response = this.controller.addTheme(this.createTheme);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(this.mockTheme, response.getBody());
		Mockito.verify(this.mockThemeService).createTheme(Mockito.any());
	}

	/**
	 * test the addTheme Function with a technical exception
	 */
	@Test
	@DisplayName("test the addTheme Function with a technical exception")
	void addThemeTechnicalException() throws Exception {
		Mockito.doThrow(TechnicalException.class).when(this.mockThemeService).createTheme(Mockito.any());
		assertThrows(TechnicalException.class, () -> this.controller.addTheme(this.createTheme));
	}

	/**
	 * test the addTheme Function with a functional exception
	 */
	@Test
	@DisplayName("test the addTheme Function with a functional exception")
	void addThemeFunctionalException() throws Exception {
		Mockito.doThrow(FunctionalException.class).when(this.mockThemeService).createTheme(Mockito.any());
		assertThrows(FunctionalException.class, () -> this.controller.addTheme(this.createTheme));
	}

	/**
	 * test the function to get all the themes
	 */
	@Test
	@DisplayName("test the function to get all the themes")
	void getThemes() throws Exception {
		final List<Theme> themeList = new ArrayList<>();
		themeList.add(this.mockTheme);
		themeList.add(this.mockTheme);
		themeList.add(this.mockTheme);
		Mockito.doReturn(themeList).when(this.mockThemeService).getThemes(Mockito.any(), Mockito.anyInt(),
				Mockito.anyInt());

		final ResponseEntity<List<Theme>> response = this.controller.getThemes(Language.FR, 0, 3);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(3, response.getBody().size());
		Mockito.verify(this.mockThemeService).getThemes(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
	}

}
