package com.gbournac.question.theme;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Class to test the context loading
 * 
 * @author Guénaël Bournac
 *
 */
@SpringBootTest
class ContextTest {

	/**
	 * Function to test the context loading
	 */
	@Test
	void contextLoads() {
	}

}
