package com.gbournac.question.theme.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.gbournac.question.common.services.exception.FunctionalException;
import com.gbournac.question.theme.domain.Language;
import com.gbournac.question.theme.domain.dto.CreateTheme;
import com.gbournac.question.theme.domain.dto.Theme;
import com.gbournac.question.theme.entity.ThemeEntity;
import com.gbournac.question.theme.exception.ExceptionStatusCode;

/**
 * The fabric service test class
 *
 * @author Guénaël Bournac
 *
 */
class FabricServiceImplTest {

	/**
	 * The fabric service under test
	 */
	FabricServiceImpl fabricService;

	@BeforeEach
	void setUp() throws Exception {
		this.fabricService = new FabricServiceImpl();
	}

	/**
	 * test the function to create entity from dto
	 */
	@ParameterizedTest
	@ValueSource(strings = { "FR", "EN" })
	@DisplayName("test the function to create entity from dto")
	void createEntityFromCreateDTO(final String languageString) throws Exception {
		final Language language = Language.valueOf(languageString);
		final CreateTheme createTheme = CreateTheme.builder().language(language).name("a").build();
		final ThemeEntity result = this.fabricService.createEntityFromCreateDTO(createTheme);
		assertTrue(result.getNames().containsKey(language));
		assertEquals("a", result.getNames().get(language));
	}

	/**
	 * test the function to create a theme dto from the entity object with one
	 * language
	 */
	@Test
	@DisplayName("test the function to create a theme dto from the entity object with one language")
	void createThemeDTOFromThemeEntityOneLanguage() throws Exception {
		final ThemeEntity themeEntity = new ThemeEntity();
		themeEntity.getNames().put(Language.FR, "a");

		final Theme theme = this.fabricService.createThemeDTOFromThemeEntity(themeEntity, Language.FR);

		assertEquals(themeEntity.getId(), theme.getId());
		assertEquals("a", theme.getName());
	}

	/**
	 * test the function to create a theme dto from the entity object with several
	 * languages
	 */
	@Test
	@DisplayName("test the function to create a theme dto from the entity object with several languages")
	void createThemeDTOFromThemeEntitySeveralLanguages() throws Exception {
		final ThemeEntity themeEntity = new ThemeEntity();
		themeEntity.getNames().put(Language.FR, "a");
		themeEntity.getNames().put(Language.EN, "b");

		final Theme theme1 = this.fabricService.createThemeDTOFromThemeEntity(themeEntity, Language.FR);

		assertEquals(themeEntity.getId(), theme1.getId());
		assertEquals("a", theme1.getName());

		final Theme theme2 = this.fabricService.createThemeDTOFromThemeEntity(themeEntity, Language.EN);

		assertEquals(themeEntity.getId(), theme2.getId());
		assertEquals("b", theme2.getName());
	}

	/**
	 * test the function to create a theme dto from the entity object with the wrong
	 * language
	 */
	@Test
	@DisplayName("test the function to create a theme dto from the entity object with one wrong language")
	void createThemeDTOFromThemeEntityWrongLanguage() throws Exception {
		final ThemeEntity themeEntity = new ThemeEntity();
		themeEntity.getNames().put(Language.FR, "a");

		final FunctionalException exception = assertThrows(FunctionalException.class,
				() -> this.fabricService.createThemeDTOFromThemeEntity(themeEntity, Language.EN));

		assertEquals(ExceptionStatusCode.INVALID_LANGUAGE, exception.getCode());
	}
}
