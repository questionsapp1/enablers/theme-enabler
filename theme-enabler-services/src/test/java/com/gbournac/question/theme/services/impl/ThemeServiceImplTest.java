package com.gbournac.question.theme.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;

import com.gbournac.question.common.services.exception.FunctionalException;
import com.gbournac.question.common.services.exception.TechnicalException;
import com.gbournac.question.theme.domain.Language;
import com.gbournac.question.theme.domain.dto.CreateTheme;
import com.gbournac.question.theme.domain.dto.Theme;
import com.gbournac.question.theme.entity.ThemeEntity;
import com.gbournac.question.theme.exception.ExceptionStatusCode;
import com.gbournac.question.theme.repository.ThemeRepository;
import com.gbournac.question.theme.services.FabricService;

/**
 * The class to test the Theme service
 *
 * @author Guénaël Bournac
 *
 */
class ThemeServiceImplTest {

	/**
	 * The service under test
	 */
	ThemeServiceImpl themeService;

	/**
	 * A mocked Theme Repository
	 */
	ThemeRepository mockThemeRepository;

	/**
	 * A mocked fabric service
	 */
	FabricService mockFabricService;

	/**
	 * A mocked Theme entity
	 */
	ThemeEntity mockThemeEntity;

	/**
	 * A mocked Create theme dto object
	 */
	CreateTheme mockCreateTheme;

	/**
	 * A mocked theme
	 */
	Theme mockTheme;

	@BeforeEach
	void setUp() throws Exception {
		this.mockCreateTheme = Mockito.mock(CreateTheme.class);
		this.mockTheme = Mockito.mock(Theme.class);

		this.mockThemeRepository = Mockito.mock(ThemeRepository.class);

		this.mockThemeEntity = Mockito.mock(ThemeEntity.class);
		Mockito.doReturn("a").when(this.mockThemeEntity).getId();

		this.mockFabricService = Mockito.mock(FabricService.class);
		Mockito.doReturn(this.mockThemeEntity).when(this.mockFabricService).createEntityFromCreateDTO(Mockito.any());
		Mockito.doReturn(this.mockTheme).when(this.mockFabricService)
				.createThemeDTOFromThemeEntity(Mockito.eq(this.mockThemeEntity), Mockito.any());

		this.themeService = new ThemeServiceImpl();
		this.themeService.setThemeRepository(this.mockThemeRepository);
		this.themeService.setFabricService(this.mockFabricService);

		this.themeService = Mockito.spy(this.themeService);
		Mockito.doNothing().when(this.themeService).setHeader(Mockito.anyString(), Mockito.anyString());
	}

	/**
	 * Test the add function in normal conditions
	 *
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	@Test
	@DisplayName("Test the add function in normal conditions")
	void testAddFunctionOK() throws TechnicalException, FunctionalException {
		Mockito.doReturn(Optional.of(this.mockThemeEntity)).when(this.mockThemeRepository)
				.findById(Mockito.anyString());
		final Theme theme = this.themeService.createTheme(this.mockCreateTheme);
		assertEquals(this.mockTheme, theme);
		Mockito.verify(this.mockFabricService).createEntityFromCreateDTO(Mockito.any());
		Mockito.verify(this.mockFabricService).createThemeDTOFromThemeEntity(Mockito.any(), Mockito.any());

		Mockito.verify(this.mockThemeRepository).save(Mockito.any());
		Mockito.verify(this.mockThemeRepository).findById(Mockito.anyString());
	}

	/**
	 * Test the add function with the FunctionalException
	 *
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	@Test
	@DisplayName("Test the add function with the FunctionalException")
	void testAddFunctionKOFunctionalException() throws TechnicalException, FunctionalException {
		Mockito.doReturn(Optional.of(this.mockThemeEntity)).when(this.mockThemeRepository)
				.findById(Mockito.anyString());
		Mockito.doThrow(FunctionalException.class).when(this.mockFabricService)
				.createThemeDTOFromThemeEntity(Mockito.any(), Mockito.any());

		assertThrows(FunctionalException.class, () -> this.themeService.createTheme(this.mockCreateTheme));

		Mockito.verify(this.mockFabricService).createEntityFromCreateDTO(Mockito.any());
		Mockito.verify(this.mockFabricService).createThemeDTOFromThemeEntity(Mockito.any(), Mockito.any());

		Mockito.verify(this.mockThemeRepository).save(Mockito.any());
		Mockito.verify(this.mockThemeRepository).findById(Mockito.anyString());
	}

	/**
	 * Test the add function with the TechnicalException
	 *
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	@Test
	@DisplayName("Test the add function with the TechnicalException")
	void testAddFunctionKOTechnicalException() throws TechnicalException, FunctionalException {
		Mockito.doReturn(Optional.empty()).when(this.mockThemeRepository).findById(Mockito.anyString());

		final TechnicalException exception = assertThrows(TechnicalException.class,
				() -> this.themeService.createTheme(this.mockCreateTheme));
		assertEquals(ExceptionStatusCode.DATA_BASE_ITEM_NOT_ADDED, exception.getCode());
		Mockito.verify(this.mockFabricService).createEntityFromCreateDTO(Mockito.any());
		Mockito.verify(this.mockFabricService, Mockito.never()).createThemeDTOFromThemeEntity(Mockito.any(),
				Mockito.any());

		Mockito.verify(this.mockThemeRepository).save(Mockito.any());
		Mockito.verify(this.mockThemeRepository).findById(Mockito.anyString());
	}

	/**
	 * test the function to get all the themes OK
	 */
	@Test
	@DisplayName("test the function to get all the themes OK")
	void getThemesOK() throws Exception {
		final List<ThemeEntity> entityList = new ArrayList<>();
		entityList.add(this.mockThemeEntity);
		entityList.add(this.mockThemeEntity);
		entityList.add(this.mockThemeEntity);
		final Page<ThemeEntity> mockPage = Mockito.mock(Page.class);
		Mockito.doReturn(entityList).when(mockPage).getContent();
		Mockito.doReturn(mockPage).when(this.mockThemeRepository).findByLanguage(Mockito.any(), Mockito.any());

		final List<Theme> themeList = this.themeService.getThemes(Language.FR, 0, 3);

		assertEquals(3, themeList.size());
		Mockito.verify(this.mockThemeRepository).findByLanguage(Mockito.any(), Mockito.any());
		Mockito.verify(this.mockFabricService, Mockito.times(3)).createThemeDTOFromThemeEntity(Mockito.any(),
				Mockito.any());
	}

	/**
	 * test the function to get all the themes with a null value
	 */
	@Test
	@DisplayName("test the function to get all the themes with a null value")
	void getThemesNullValue() throws Exception {
		final List<ThemeEntity> entityList = new ArrayList<>();
		entityList.add(this.mockThemeEntity);
		entityList.add(this.mockThemeEntity);
		entityList.add(this.mockThemeEntity);
		entityList.add(null);
		final Page<ThemeEntity> mockPage = Mockito.mock(Page.class);
		Mockito.doReturn(entityList).when(mockPage).getContent();
		Mockito.doReturn(mockPage).when(this.mockThemeRepository).findByLanguage(Mockito.any(), Mockito.any());

		final List<Theme> themeList = this.themeService.getThemes(Language.FR, 0, 3);

		assertEquals(3, themeList.size());
		Mockito.verify(this.mockThemeRepository).findByLanguage(Mockito.any(), Mockito.any());
		Mockito.verify(this.mockFabricService, Mockito.times(3))
				.createThemeDTOFromThemeEntity(Mockito.eq(this.mockThemeEntity), Mockito.any());
	}

	/**
	 * test the function to get all the themes with values in an other language
	 */
	@Test
	@DisplayName("test the function to get all the themes with a null value")
	void getThemesOtherLanguage() throws Exception {
		final List<ThemeEntity> entityList = new ArrayList<>();
		entityList.add(this.mockThemeEntity);
		entityList.add(this.mockThemeEntity);
		entityList.add(this.mockThemeEntity);
		final ThemeEntity invalidEntity = Mockito.mock(ThemeEntity.class);
		entityList.add(invalidEntity);
		final Page<ThemeEntity> mockPage = Mockito.mock(Page.class);
		Mockito.doReturn(entityList).when(mockPage).getContent();
		Mockito.doReturn(mockPage).when(this.mockThemeRepository).findByLanguage(Mockito.any(), Mockito.any());

		Mockito.doThrow(FunctionalException.class).when(this.mockFabricService)
				.createThemeDTOFromThemeEntity(Mockito.eq(invalidEntity), Mockito.any());

		final List<Theme> themeList = this.themeService.getThemes(Language.FR, 0, 3);

		assertEquals(3, themeList.size());
		Mockito.verify(this.mockThemeRepository).findByLanguage(Mockito.any(), Mockito.any());
		Mockito.verify(this.mockFabricService, Mockito.times(3))
				.createThemeDTOFromThemeEntity(Mockito.eq(this.mockThemeEntity), Mockito.any());
	}

	/**
	 * test the function to get a theme by it's id in normal condition
	 */
	@Test
	@DisplayName("test the function to get a theme by it's id in normal condition")
	void getThemeByIdOK() throws Exception {

		Mockito.doReturn(Optional.of(this.mockThemeEntity)).when(this.mockThemeRepository)
				.findById(Mockito.anyString());

		final Theme theme = this.themeService.getThemeById(Language.FR, "a");

		Mockito.verify(this.mockFabricService).createThemeDTOFromThemeEntity(Mockito.any(), Mockito.eq(Language.FR));
		Mockito.verify(this.mockThemeRepository).findById(Mockito.anyString());
		assertNotNull(theme);
	}

	/**
	 * test the function to get a theme when the element isn't in the database
	 */
	@Test
	@DisplayName("test the function to get a theme when the element isn't in the database")
	void getThemeByIdNotInDatabase() throws Exception {
		Mockito.doReturn(Optional.empty()).when(this.mockThemeRepository).findById(Mockito.anyString());

		final FunctionalException exception = assertThrows(FunctionalException.class,
				() -> this.themeService.getThemeById(Language.FR, "a"));
		assertEquals(ExceptionStatusCode.DATA_BASE_ITEM_NOT_PRESENT, exception.getCode());

		Mockito.verify(this.mockFabricService, Mockito.never()).createThemeDTOFromThemeEntity(Mockito.any(),
				Mockito.eq(Language.FR));
		Mockito.verify(this.mockThemeRepository).findById(Mockito.anyString());
	}

	/**
	 * test the function to get a theme when the language isn't in the database
	 */
	@Test
	@DisplayName("test the function to get a theme when the language isn't in the database")
	void getThemeByIdNotLanguage() throws Exception {
		Mockito.doReturn(Optional.of(this.mockThemeEntity)).when(this.mockThemeRepository)
				.findById(Mockito.anyString());
		Mockito.doThrow(FunctionalException.class).when(this.mockFabricService)
				.createThemeDTOFromThemeEntity(Mockito.any(), Mockito.any());

		final FunctionalException exception = assertThrows(FunctionalException.class,
				() -> this.themeService.getThemeById(Language.FR, "a"));

		Mockito.verify(this.mockFabricService).createThemeDTOFromThemeEntity(Mockito.any(), Mockito.eq(Language.FR));
		Mockito.verify(this.mockThemeRepository).findById(Mockito.anyString());
	}
}
