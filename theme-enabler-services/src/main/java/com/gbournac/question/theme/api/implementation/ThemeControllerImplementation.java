package com.gbournac.question.theme.api.implementation;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.gbournac.question.common.services.exception.FunctionalException;
import com.gbournac.question.common.services.exception.TechnicalException;
import com.gbournac.question.common.services.logger.LogFormater;
import com.gbournac.question.theme.domain.Language;
import com.gbournac.question.theme.domain.api.ThemeController;
import com.gbournac.question.theme.domain.dto.CreateTheme;
import com.gbournac.question.theme.domain.dto.Theme;
import com.gbournac.question.theme.services.ThemeService;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * The theme controller implementation
 *
 * @author Guénaël Bournac
 *
 */
@RestController
@Slf4j
@Setter
public class ThemeControllerImplementation implements ThemeController {

	/**
	 * The theme service to manage the themes
	 */
	@Autowired
	private ThemeService themeService;

	/**
	 * {@inheritDoc}
	 *
	 * @throws FunctionalException
	 */
	@Override
	public ResponseEntity<Theme> addTheme(@Valid final CreateTheme newTheme)
			throws TechnicalException, FunctionalException {
		final String sMethodName = "addTheme";
		LogFormater.startFunction(log, sMethodName, "");

		LogFormater.info(log, sMethodName, "Add a new theme '{}' in '{}'", newTheme.getName(), newTheme.getLanguage());
		final Theme result = this.themeService.createTheme(newTheme);

		LogFormater.endFunction(log, sMethodName, "");
		return ResponseEntity.ok(result);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseEntity<List<Theme>> getThemes(final Language language, final int page, final int size) {
		final String sMethodName = "getThemes";
		LogFormater.startFunction(log, sMethodName, "");

		LogFormater.info(log, sMethodName, "Get all the themes");
		final List<Theme> resultBody = this.themeService.getThemes(language, page, size);

		LogFormater.endFunction(log, sMethodName, "");
		return ResponseEntity.ok(resultBody);
	}

	@Override
	public ResponseEntity<Theme> getTheme(final Language language, final String id) throws FunctionalException {
		final String sMethodName = "getTheme";
		LogFormater.startFunction(log, sMethodName, "");

		final Theme theme = this.themeService.getThemeById(language, id);

		LogFormater.endFunction(log, sMethodName, "");
		return ResponseEntity.ok(theme);
	}
}
