package com.gbournac.question.theme.exception;

import org.springframework.http.HttpStatus;

import com.gbournac.question.common.services.exception.ExceptionCode;

/**
 * The exception status code enumeration
 *
 * @author Guénaël Bournac
 *
 */
public enum ExceptionStatusCode implements ExceptionCode {

	// Database: 1**
	/**
	 * the data base common exception status
	 */
	DATA_BASE_COMMON("100", HttpStatus.INTERNAL_SERVER_ERROR),

	/**
	 * The status when the item havn't been added
	 */
	DATA_BASE_ITEM_NOT_ADDED("101", HttpStatus.INTERNAL_SERVER_ERROR),

	/**
	 * If the item isn't in the database
	 */
	DATA_BASE_ITEM_NOT_PRESENT("102", HttpStatus.NOT_FOUND),

	// Input: 2**
	/**
	 * The status when the inputs are invalid
	 */
	INPUT_NOT_VALID("200", HttpStatus.BAD_REQUEST),

	// Language: 3**
	/**
	 * The status when the language is invalid
	 */
	INVALID_LANGUAGE("300", HttpStatus.BAD_REQUEST);

	/**
	 * The internal error code
	 */
	private String code;

	/**
	 * The http status code
	 */
	private HttpStatus statusCode;

	/**
	 * Constructor
	 *
	 * @param code
	 * @param statusCode
	 */
	ExceptionStatusCode(final String code, final HttpStatus statusCode) {
		this.code = code;
		this.statusCode = statusCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInternalProjectCode() {
		return "TE" + this.code;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpStatus getHTTPStatusCode() {
		return this.statusCode;
	}

}
