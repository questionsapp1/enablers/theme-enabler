package com.gbournac.question.theme.services.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.gbournac.question.common.services.exception.FunctionalException;
import com.gbournac.question.common.services.exception.TechnicalException;
import com.gbournac.question.common.services.logger.LogFormater;
import com.gbournac.question.theme.domain.Language;
import com.gbournac.question.theme.domain.dto.CreateTheme;
import com.gbournac.question.theme.domain.dto.Theme;
import com.gbournac.question.theme.entity.ThemeEntity;
import com.gbournac.question.theme.exception.ExceptionStatusCode;
import com.gbournac.question.theme.repository.ThemeRepository;
import com.gbournac.question.theme.services.FabricService;
import com.gbournac.question.theme.services.ThemeService;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * The theme service implementation
 *
 * @author Guénaël Bournac
 *
 */
@Service
@Setter
@Slf4j
public class ThemeServiceImpl implements ThemeService {

	/**
	 * The theme repository to manage the database
	 */
	@Autowired
	private ThemeRepository themeRepository;

	/**
	 * The fabric function to change entity and dto
	 */
	@Autowired
	private FabricService fabricService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Theme createTheme(final CreateTheme newTheme) throws TechnicalException, FunctionalException {
		final String sMethodName = "createTheme";
		LogFormater.startFunction(log, sMethodName, "");

		final ThemeEntity themeEntity = this.fabricService.createEntityFromCreateDTO(newTheme);

		LogFormater.debug(log, sMethodName, "Save the entity");
		this.themeRepository.save(themeEntity);

		LogFormater.debug(log, sMethodName, "Get the entity");
		final Optional<ThemeEntity> addedThemeEntity = this.themeRepository.findById(themeEntity.getId());

		if (!addedThemeEntity.isPresent()) {
			LogFormater.error(log, sMethodName, "The item isn't present after it has been added");
			throw new TechnicalException(ExceptionStatusCode.DATA_BASE_ITEM_NOT_ADDED);
		}

		final Theme result = this.fabricService.createThemeDTOFromThemeEntity(addedThemeEntity.get(),
				newTheme.getLanguage());

		LogFormater.endFunction(log, sMethodName, "");
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Theme> getThemes(final Language language, final int page, final int size) {
		final String sMethodName = "getThemes";
		LogFormater.startFunction(log, sMethodName, "");

		final Pageable paging = PageRequest.of(page, size);
		final Page<ThemeEntity> themePage = this.themeRepository.findByLanguage(language, paging);
		this.setHeader("X-PAGE-SELF", themePage.getNumber() + "");
		this.setHeader("X-PAGE-MAX", themePage.getTotalPages() - 1 + "");
		this.setHeader("X-PAGE-SIZE", themePage.getSize() + "");

		final List<ThemeEntity> themeList = themePage.getContent();

		final List<Theme> result = themeList.stream().filter(themeEntity -> themeEntity != null).map(themeEntity -> {
			try {
				return this.fabricService.createThemeDTOFromThemeEntity(themeEntity, language);
			} catch (final FunctionalException e) {
				LogFormater.error(log, sMethodName, "Error when getting the langage for the theme {}",
						themeEntity.getId());
			}
			return null;
		}).filter(theme -> theme != null).collect(Collectors.toList());

		LogFormater.endFunction(log, sMethodName, "");
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Theme getThemeById(final Language language, final String id) throws FunctionalException {
		final String sMethodName = "getThemeById";
		LogFormater.startFunction(log, sMethodName, "");

		final Optional<ThemeEntity> themeO = this.themeRepository.findById(id);

		if (!themeO.isPresent()) {
			LogFormater.error(log, sMethodName, "The theme with the id {} isn't in the database", id);
			throw new FunctionalException(ExceptionStatusCode.DATA_BASE_ITEM_NOT_PRESENT,
					"The theme with the id " + id + " isn't in the database");
		}
		final ThemeEntity themeE = themeO.get();

		final Theme theme = this.fabricService.createThemeDTOFromThemeEntity(themeE, language);

		LogFormater.endFunction(log, sMethodName, "");
		return theme;
	}

	protected void setHeader(final String key, final String value) {
		final String sMethodName = "setHeader";
		LogFormater.startFunction(log, sMethodName, "");

		final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		final HttpServletResponse response = ((ServletRequestAttributes) requestAttributes).getResponse();
		response.setHeader(key, value);

		LogFormater.endFunction(log, sMethodName, "");

	}

}
