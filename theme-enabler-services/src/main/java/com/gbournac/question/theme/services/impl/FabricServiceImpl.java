package com.gbournac.question.theme.services.impl;

import org.springframework.stereotype.Service;

import com.gbournac.question.common.services.exception.FunctionalException;
import com.gbournac.question.common.services.logger.LogFormater;
import com.gbournac.question.theme.domain.Language;
import com.gbournac.question.theme.domain.dto.CreateTheme;
import com.gbournac.question.theme.domain.dto.Theme;
import com.gbournac.question.theme.entity.ThemeEntity;
import com.gbournac.question.theme.exception.ExceptionStatusCode;
import com.gbournac.question.theme.services.FabricService;

import lombok.extern.slf4j.Slf4j;

/**
 * The fabric implementation
 *
 * @author Guénaël Bournac
 *
 */
@Service
@Slf4j
public class FabricServiceImpl implements FabricService {

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public ThemeEntity createEntityFromCreateDTO(final CreateTheme createDTO) {
		final String sMethodName = "createEntityFromCreateDTO";
		LogFormater.startFunction(log, sMethodName, "");

		final ThemeEntity result = new ThemeEntity();
		result.getNames().put(createDTO.getLanguage(), createDTO.getName());

		LogFormater.endFunction(log, sMethodName, "");
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Theme createThemeDTOFromThemeEntity(final ThemeEntity themeEntity, final Language language)
			throws FunctionalException {
		final String sMethodName = "createThemeDTOFromThemeEntity";
		LogFormater.startFunction(log, sMethodName, "");

		if (!themeEntity.getNames().containsKey(language)) {
			LogFormater.error(log, sMethodName, "[{}] The language {} is invalid for the theme {}",
					ExceptionStatusCode.INVALID_LANGUAGE.getInternalProjectCode(), language.name(),
					themeEntity.getId());
			throw new FunctionalException(ExceptionStatusCode.INVALID_LANGUAGE,
					"The language " + language.name() + " is invalid for the theme " + themeEntity.getId());
		}

		final Theme result = new Theme();
		result.setId(themeEntity.getId());
		result.setName(themeEntity.getNames().get(language));

		LogFormater.endFunction(log, sMethodName, "");
		return result;
	}
}
