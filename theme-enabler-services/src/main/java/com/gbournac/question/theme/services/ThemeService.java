package com.gbournac.question.theme.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gbournac.question.common.services.exception.FunctionalException;
import com.gbournac.question.common.services.exception.TechnicalException;
import com.gbournac.question.theme.domain.Language;
import com.gbournac.question.theme.domain.dto.CreateTheme;
import com.gbournac.question.theme.domain.dto.Theme;

/**
 * The service to call the repository functions
 *
 * @author Guénaël Bournac
 *
 */
@Service
public interface ThemeService {

	/**
	 * Function to save the theme in a database and to return the added theme
	 *
	 * @param newTheme {@link CreateTheme}
	 * @return {@link Theme}
	 * @throws TechnicalException
	 * @throws FunctionalException
	 */
	Theme createTheme(CreateTheme newTheme) throws TechnicalException, FunctionalException;

	/**
	 * Function to list all the themes
	 *
	 * @param language
	 * @param page
	 * @param size
	 * @return {@link Theme}
	 */
	List<Theme> getThemes(Language language, int page, int size);

	/**
	 * To get one item by the it's id and the language
	 *
	 * @param language
	 * @param id
	 * @return
	 * @throws FunctionalException
	 */
	Theme getThemeById(Language language, String id) throws FunctionalException;
}
