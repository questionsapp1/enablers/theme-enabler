package com.gbournac.question.theme.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.gbournac.question.theme.domain.Language;
import com.gbournac.question.theme.entity.ThemeEntity;

/**
 * The theme repository to manage themes with the mongo database
 *
 * @author Guénaël Bournac
 *
 */
@Repository
public interface ThemeRepository extends MongoRepository<ThemeEntity, String> {

	@Query("{ 'names.?0' : { '$exists': true } }")
	List<ThemeEntity> findByLanguage(Language language);

	@Query("{ 'names.?0' : { '$exists': true } }")
	Page<ThemeEntity> findByLanguage(Language language, Pageable pageable);

}
