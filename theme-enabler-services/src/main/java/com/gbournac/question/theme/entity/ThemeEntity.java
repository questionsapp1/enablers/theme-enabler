package com.gbournac.question.theme.entity;

import java.util.EnumMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.gbournac.question.theme.domain.Language;

import lombok.Data;

/**
 *
 * The theme entity to save it in a database
 *
 * @author Guénaël Bournac
 *
 */
@Document(collection = "theme")
@Data
public class ThemeEntity {

	/**
	 * The theme id
	 */
	@Id
	private String id;

	/**
	 * A map with the theme names for example { FR => Chat, EN => Cat}
	 */
	private Map<Language, String> names;

	/**
	 * The constructor to initialize the map
	 */
	public ThemeEntity() {
		this.names = new EnumMap<>(Language.class);
	}

}
