package com.gbournac.question.theme;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.gbournac.question.common.services.interceptor.CorrelationInterceptor;
import com.gbournac.question.common.services.logger.LogFormater;

@SpringBootApplication
@Configuration
public class ThemeEnabler implements WebMvcConfigurer {

	public static void main(final String[] args) {
		SpringApplication.run(ThemeEnabler.class, args);
	}

	@Autowired
	private RequestInterceptor requestInterceptor;

	@Override
	public void addInterceptors(final InterceptorRegistry registry) {
		registry.addInterceptor(this.requestInterceptor).addPathPatterns("/**");
	}

	@Bean
	public CorrelationInterceptor getCorrelationInterceptor() {
		return new CorrelationInterceptor();
	}

	@Component
	private static class RequestInterceptor extends HandlerInterceptorAdapter {

		/**
		 * Logger
		 */
		private static final Logger logger = LoggerFactory.getLogger(RequestInterceptor.class);

		@Autowired
		private CorrelationInterceptor interceptor;

		@Override
		public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response,
				final Object handler) {
			this.interceptor.addCorrelationID(request);
			LogFormater.beforeRequestLog(logger, request);
			return true;
		}

		@Override
		public void afterCompletion(final HttpServletRequest request, final HttpServletResponse response,
				final Object handler, final Exception ex) {
			LogFormater.afterRequestLog(logger, request, response);
		}
	}
}
