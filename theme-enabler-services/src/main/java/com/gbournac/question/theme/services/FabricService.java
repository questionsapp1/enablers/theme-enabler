package com.gbournac.question.theme.services;

import org.springframework.stereotype.Service;

import com.gbournac.question.common.services.exception.FunctionalException;
import com.gbournac.question.theme.domain.Language;
import com.gbournac.question.theme.domain.dto.CreateTheme;
import com.gbournac.question.theme.domain.dto.Theme;
import com.gbournac.question.theme.entity.ThemeEntity;

/**
 * The service to change from dto to entity and from entity to dto
 *
 * @author Guénaël Bournac
 *
 */
@Service
public interface FabricService {

	/**
	 * The function create a theme entity from a create theme dto
	 *
	 * @param createDTO {@link CreateTheme}
	 * @return {@link ThemeEntity}
	 */
	ThemeEntity createEntityFromCreateDTO(CreateTheme createDTO);

	/**
	 * The function to create a theme DTO from a theme entity
	 *
	 * @param themeEntity {@link ThemeEntity}
	 * @param language
	 * @return {@link Theme}
	 * @throws FunctionalException
	 */
	Theme createThemeDTOFromThemeEntity(ThemeEntity themeEntity, Language language) throws FunctionalException;

}
