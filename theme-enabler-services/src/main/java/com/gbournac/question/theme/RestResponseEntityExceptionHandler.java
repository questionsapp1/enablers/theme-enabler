package com.gbournac.question.theme;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.gbournac.question.common.domain.dto.ErrorMessageDTO;
import com.gbournac.question.common.services.exception.DTOConstructor;
import com.gbournac.question.common.services.exception.FunctionalException;
import com.gbournac.question.common.services.exception.ProjectException;
import com.gbournac.question.theme.exception.ExceptionStatusCode;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { ProjectException.class })
	protected ResponseEntity<ErrorMessageDTO> handleProjectException(final ProjectException ex,
			final WebRequest request) {
		final ErrorMessageDTO errorMessage = DTOConstructor.createDTO(ex);
		return ResponseEntity.status(ex.getCode().getHTTPStatusCode()).body(errorMessage);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Invalid fields: ");
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			final String fieldName = ((FieldError) error).getField();
			stringBuilder.append(fieldName + ", ");
		});
		final String message = stringBuilder.toString();

		final ProjectException projectException = new FunctionalException(ExceptionStatusCode.INPUT_NOT_VALID, message);
		final ErrorMessageDTO errorMessage = DTOConstructor.createDTO(projectException);
		return ResponseEntity.status(projectException.getCode().getHTTPStatusCode()).body(errorMessage);
	}

	@ExceptionHandler(value = { ConstraintViolationException.class })
	protected ResponseEntity<Object> handleConstraintViolationException(final ConstraintViolationException ex,
			final WebRequest request) {
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Invalid fields: ");
		ex.getConstraintViolations().forEach((error) -> {
			final String fieldName = error.getPropertyPath().toString();
			stringBuilder.append(fieldName + ", ");
		});
		final String message = stringBuilder.toString();

		final ProjectException projectException = new FunctionalException(ExceptionStatusCode.INPUT_NOT_VALID, message);
		final ErrorMessageDTO errorMessage = DTOConstructor.createDTO(projectException);
		return ResponseEntity.status(projectException.getCode().getHTTPStatusCode()).body(errorMessage);
	}
}
