package com.gbournac.question.theme.domain.constant;

/**
 * The theme enabler root urls
 *
 * @author Guénaël Bournac
 *
 */
public class RootUrl {

	/**
	 * private constructor
	 */
	private RootUrl() {
	}

	/**
	 * The theme enabler url
	 */
	public static final String THEME_ENABLER = "/theme-enabler";

	/**
	 * The version url
	 */
	public static final String VERSION = "/v0";

	/**
	 * The root url
	 */
	public static final String ROOT = THEME_ENABLER + VERSION;
}
