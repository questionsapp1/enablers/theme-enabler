package com.gbournac.question.theme.domain.constant;

/**
 * The theme controller url
 *
 * @author Guénaël Bournac
 *
 */
public class ThemeUrl {

	/**
	 * private constructor
	 */
	private ThemeUrl() {
	}

	/**
	 * The theme controller main url
	 */
	public static final String THEME = RootUrl.ROOT + "/themes";
}
