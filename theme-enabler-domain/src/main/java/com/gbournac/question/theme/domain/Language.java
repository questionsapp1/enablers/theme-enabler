package com.gbournac.question.theme.domain;

/**
 * Enum containing all the accepted languages
 *
 * @author Guénaël Bournac
 *
 */
public enum Language {
	/**
	 * French
	 */
	FR,
	/**
	 * English
	 */
	EN
}
