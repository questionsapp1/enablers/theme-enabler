package com.gbournac.question.theme.domain.constant;

/**
 * default values for query parameters
 *
 * @author Guénaël Bournac
 *
 */
public class QueryDefaultParameter {

	/**
	 * The default language
	 */
	public static final String DEFAULT_LANGUAGE = "FR";

	/**
	 * THe default page number
	 */
	public static final String DEFAULT_PAGE_NUMBER = "0";

	/**
	 * The default page size
	 */
	public static final String DEFAULT_PAGE_SIZE = "3";

	/**
	 * Private constructor
	 */
	private QueryDefaultParameter() {

	}
}
