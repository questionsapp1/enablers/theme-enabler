package com.gbournac.question.theme.domain.api;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gbournac.question.common.services.exception.FunctionalException;
import com.gbournac.question.common.services.exception.ProjectException;
import com.gbournac.question.theme.domain.Language;
import com.gbournac.question.theme.domain.constant.QueryDefaultParameter;
import com.gbournac.question.theme.domain.constant.ThemeUrl;
import com.gbournac.question.theme.domain.dto.CreateTheme;
import com.gbournac.question.theme.domain.dto.Theme;

/**
 * The controller used for the theme question management
 *
 * @author Guénaël Bournac
 */
@RequestMapping(ThemeUrl.THEME)
@Validated
public interface ThemeController {

	/**
	 * The function to add a new Theme
	 *
	 * @return {@link Theme} the new theme
	 * @throws ProjectException
	 */
	@PostMapping
	ResponseEntity<Theme> addTheme(@Valid @RequestBody CreateTheme newTheme) throws ProjectException;

	/**
	 * The function to get all the themes
	 *
	 * @return
	 */
	@GetMapping
	ResponseEntity<List<Theme>> getThemes(
			@RequestParam(name = "lang", defaultValue = QueryDefaultParameter.DEFAULT_LANGUAGE) Language language,
			@RequestParam(name = "page", defaultValue = "0") int page,
			@Max(100) @RequestParam(name = "size", defaultValue = "3") int size);

	/**
	 * Function to get one theme by it's id
	 *
	 * @param id
	 * @return
	 * @throws FunctionalException
	 */
	@GetMapping("/{id}")
	ResponseEntity<Theme> getTheme(
			@RequestParam(name = "lang", defaultValue = QueryDefaultParameter.DEFAULT_LANGUAGE) Language language,
			@PathVariable("id") String id) throws FunctionalException;

}
