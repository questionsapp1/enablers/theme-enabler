package com.gbournac.question.theme.domain.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.gbournac.question.theme.domain.Language;

import lombok.Builder;
import lombok.Data;

/**
 * The DTO to create a new Theme
 */
@Data
@Builder
public class CreateTheme {

	/**
	 * The theme name
	 */
	@NotNull
	@Size(min = 1, max = 20)
	private String name;

	/**
	 * The theme language
	 */
	@NotNull
	// @Pattern(regexp = "^(FR|EN)$")
	private Language language;
}
