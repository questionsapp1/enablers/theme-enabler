package com.gbournac.question.theme.domain.dto;

import lombok.Data;

/**
 * The DTO to return themes
 *
 * @author Guénaël Bournac
 *
 */
@Data
public class Theme {

	/**
	 * The theme ID
	 */
	private String id;

	/**
	 * The theme name
	 */
	private String name;
}
