package com.gbournac.question.theme.domain.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.gbournac.question.theme.domain.Language;

/**
 * To test the validation
 *
 * @author Guénaël Bournac
 *
 */
class CreateThemeTest {

	/**
	 * A valid language String
	 */
	private static final Language VALID_LANGUAGE = Language.FR;

	/**
	 * A valid name string
	 */
	private static final String VALID_NAME_1 = "a";

	/**
	 * A valid name string
	 */
	private static final String VALID_NAME_2 = "aaaaaaaaa aaaaaaaaaa";

	/**
	 * The validator object
	 */
	private Validator validator;

	/**
	 * To initialise the validator
	 *
	 * @throws Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		this.validator = factory.getValidator();
	}

	/**
	 * Test the validation with a valid create Theme
	 *
	 * @param name
	 */
	@ParameterizedTest
	@ValueSource(strings = { VALID_NAME_1, VALID_NAME_2 })
	@DisplayName("Test the validation with a valid create Theme")
	void testValidTheme(final String name) {
		final CreateTheme theme = CreateTheme.builder().language(VALID_LANGUAGE).name(name).build();
		final Set<ConstraintViolation<CreateTheme>> violations = this.validator.validate(theme);
		assertTrue(violations.isEmpty());
	}

	/**
	 * Test the validation with an invalid name
	 */
	@ParameterizedTest
	@DisplayName("Test the validation with an invalid name")
	@ValueSource(strings = { "", "aaaaaaaaa aaaaaaaaaaa" })
	void testInvalidName(final String name) throws Exception {
		final CreateTheme theme = CreateTheme.builder().language(VALID_LANGUAGE).name(name).build();
		final Set<ConstraintViolation<CreateTheme>> violations = this.validator.validate(theme);
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
	}

	/**
	 * Test the validation with a null name
	 */
	@Test
	@DisplayName("Test the validation with a null name")
	void testNullName() throws Exception {
		final CreateTheme theme = CreateTheme.builder().language(VALID_LANGUAGE).name(null).build();
		final Set<ConstraintViolation<CreateTheme>> violations = this.validator.validate(theme);
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
	}

	/**
	 * Test the validation with a null language
	 */
	@Test
	@DisplayName("Test the validation with a null name")
	void testNullLanguage() throws Exception {
		final CreateTheme theme = CreateTheme.builder().language(null).name(VALID_NAME_1).build();
		final Set<ConstraintViolation<CreateTheme>> violations = this.validator.validate(theme);
		assertFalse(violations.isEmpty());
		assertEquals(1, violations.size());
	}
}
